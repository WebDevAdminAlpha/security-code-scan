package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"reflect"
	"strings"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/pathfilter"
)

func TestFindSolution(t *testing.T) {
	tests := []struct {
		name     string
		tree     []string
		expected []string
	}{
		{
			name: "simple",
			tree: []string{
				"foo/simple.sln",
				"foo/bar/a.tmp",
				"foo/bar/b.tmp",
				"foo/bar/c.csproj",
				"foo/bar/baz/a.tmp",
				"foo/bar/baz/b.tmp",
			},
			expected: []string{"foo/simple.sln"},
		},
		{
			name: "multi-solution",
			tree: []string{
				"foo/simple.sln",
				"foo/common.sln",
				"foo/bar/a.tmp",
				"foo/bar/b.tmp",
				"foo/bar/c.csproj",
				"foo/bar/nested.sln",
				"foo/bar/baz/a.tmp",
				"foo/bar/baz/b.tmp",
				"foo/bar/baz/c.csproj",
			},
			expected: []string{"foo/bar/nested.sln", "foo/common.sln", "foo/simple.sln"},
		},
	}
	for _, test := range tests {
		tmpDir, err := prepareTestDirTree(test.tree)
		defer os.RemoveAll(tmpDir)
		if err != nil {
			t.Fatalf("unable to create test dir tree: %v\n", err)
		}
		filter, err := createFilter(tmpDir, []string{})
		if err != nil {
			t.Fatal(err)
		}
		got, err := findSolutions(tmpDir, filter)
		if err != nil {
			t.Fatal(err)
		}

		if err != nil {
			t.Fatal(err)
		}

        var expected []string
        for _, slns := range test.expected {
            expected = append(expected, filepath.Join(tmpDir, slns))
        }

        if !reflect.DeepEqual(expected, got) {
            t.Errorf("Expected match to be\n%#v\nbut got\n%#v", expected, got)
        }
	}
}

func TestFindProjects(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name        string
		filterPaths []string
		tree        []string
		expected    []string
	}{
		{"multiproject",
			[]string{},
			[]string{
				"MultiprojectDirTreeFixture.sln",
				"a/bar/bar.vbproj",
				"a/bar/bar.cbproj",
				"a/bar/bar.sln",
				"a/bar/double/nested/proj.csproj",
				"a/bar/deeply/nested/file.txt",
				"a/foo/foo.csproj",
				"a/some_file.txt",
				"b/b.csproj",
				"c/c.sln",
			}, []string{
				"a/bar/bar.vbproj",
				"a/foo/foo.csproj",
				"b/b.csproj",
			}},
		{"singleproject",
			[]string{},
			[]string{
				"bar/bar.csproj",
			}, []string{
				"bar/bar.csproj",
			}},
		{"nonproject",
			[]string{},
			[]string{
				"bar/bar.txt",
			}, []string{}},
		{"projectWithExcludes",
			[]string{
				"TMP_DIR", // Shouldn't affect expectations if we're filtering on relative paths.
				"b/b.csproj",
				"*.vbproj",
				"c",
			},
			[]string{
				"MultiprojectDirTreeFixture.sln",
				"a/bar/bar.vbproj",
				"a/bar/double/nested/proj.csproj",
				"a/foo/foo.csproj",
				"b/b.csproj",
				"c/bar/bar.vbproj",
				"c/bar/double/nested/proj.csproj",
				"c/foo/foo.csproj",
			}, []string{
				"a/bar/double/nested/proj.csproj",
				"a/foo/foo.csproj",
			}},
	}

	for _, tt := range tests {
		tt := tt // NOTE: https://github.com/golang/go/wiki/CommonMistakes#using-goroutines-on-loop-iterator-variables
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			tmpDir, err := prepareTestDirTree(tt.tree)
			defer os.RemoveAll(tmpDir)
			if err != nil {
				t.Fatalf("unable to create test dir tree: %v\n", err)
			}

			var expected []string
			for _, projectFile := range tt.expected {
				expected = append(expected, filepath.Join(tmpDir, projectFile))
			}

			filter, err := createFilter(tmpDir, tt.filterPaths)
			if err != nil {
				t.Fatal(err)
			}

			got, err := findProjects(tmpDir, filter)
			if err != nil {
				t.Fatal(err)
			}

			if !reflect.DeepEqual(expected, got) {
				t.Errorf("Expected match to be\n%#v\nbut got\n%#v", expected, got)
			}
		})
	}
}

func prepareTestDirTree(files []string) (string, error) {
	tmpDir, err := ioutil.TempDir("", "")
	if err != nil {
		return "", err
	}

	for _, file := range files {
		dir := filepath.Dir(file)

		err = os.MkdirAll(filepath.Join(tmpDir, dir), 0755)
		if err != nil {
			return "", err
		}

		_, err = os.Create(filepath.Join(tmpDir, file))
		if err != nil {
			return "", err
		}
	}

	return tmpDir, nil
}

func createFilter(tmpDir string, paths []string) (*pathfilter.Filter, error) {
	fullPaths := make([]string, len(paths))

	for i, p := range paths {
		if _, err := filepath.Match(p, "x"); err != nil {
			return nil, fmt.Errorf("Couldn't Create Filter: %s", p)
		}

		fullPaths[i] = strings.Replace(p, "TMP_DIR", tmpDir, 1)
	}

	return &pathfilter.Filter{ExcludedPaths: fullPaths}, nil
}
