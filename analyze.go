package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/pathfilter"
	"gitlab.com/gitlab-org/security-products/analyzers/security-code-scan/v2/project"
)

const (
	nugetCmd                    = "nuget"
	msbuildCmd                  = "msbuild"
	dotnetCmd                   = "dotnet"
	pkgSecurityCodeScan         = "SecurityCodeScan"
	flagSecurityCodeScanVersion = "security-code-scan-version"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:    flagSecurityCodeScanVersion,
			Usage:   "Version of SecurityCodeScan",
			EnvVars: []string{"SECURITY_CODE_SCAN_VERSION"},
		},
	}
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	var ioArr []io.Reader

	filter, err := pathfilter.NewFilter(c)
	if err != nil {
		return nil, err
	}

	solutions, err := findSolutions(path, filter)
	if err != nil {
		return nil, err
	}
	if len(solutions) == 0 {
		projects, err := findProjects(path, filter)
		if err != nil {
			return nil, err
		}

		for _, projectPath := range projects {
			analyzeIo, err := analyzeProjectDotNet(c, projectPath)
			if err != nil {
				return nil, err
			}
			ioArr = append(ioArr, analyzeIo)
		}
	} else {
		for _, solutionPath := range solutions {
			analyzeIo, err := analyzeSolution(c, solutionPath)
			if err != nil {
				return nil, err
			}
			ioArr = append(ioArr, analyzeIo)
		}

	}

	return ioutil.NopCloser(io.MultiReader(ioArr...)), nil
}

func projectsFromSolution(solutionPath string) ([]string, error) {
	var (
		projects     []string
		shouldAppend bool
	)
	out, err := run(dotnetCmd, []string{"sln", solutionPath, "list"}, false)
	if err != nil {
		return projects, err
	}

	scanner := bufio.NewScanner(bytes.NewReader(out))
	for scanner.Scan() {
		line := scanner.Text()
		if shouldAppend {
			projects = append(projects, filepath.Join(filepath.Dir(solutionPath), scanner.Text()))
		}

		if line == "----------" {
			shouldAppend = true
		}
	}
	return projects, nil
}

// analyzeSolution accepts an absolute solution path which is used to extract projects associated with
// that solution using the `dotnet sln <solutionPath> list` command. From there we attempt to analyze
// those projects using `dotnet clean/build`. If that fails, we use `nuget` and `msbuild` to fetch
// dependencies and build the project.
func analyzeSolution(c *cli.Context, solutionPath string) (io.Reader, error) {
	var ioArr []io.Reader
	projects, err := projectsFromSolution(solutionPath)
	if err != nil {
		return nil, err
	}

	for _, projectPath := range projects {
		out, err := analyzeProjectDotNet(c, projectPath)
		if err != nil {
			log.Warn("Unable to build project using `dotnet`, attempting to build using `nuget` and `msbuild`")
			out, err = analyzeProjectMSBuild(c, projectPath, solutionPath)
			if err != nil {
				return nil, fmt.Errorf("Unable to build project from solution file using `nuget and `msbuild`.\noutput: %s\nerror: %v", out, err)
			}
		}
		ioArr = append(ioArr, out)
	}

	return io.MultiReader(ioArr...), nil
}

func analyzeProjectMSBuild(c *cli.Context, projectPath, solutionPath string) (io.Reader, error) {
	if err := project.InsertAnalyzersToProjFile(projectPath); err != nil {
		return nil, err
	}

	// restore project packages using nuget
	out, err := run(nugetCmd, []string{"restore", solutionPath}, false)
	if err != nil {
		log.Debugf("`nuget restore error:` %s", out)
		return nil, fmt.Errorf("Error %s on nuget restore:\n%s", err, out)
	}

	// build using msbuild
	out, err = run(msbuildCmd, []string{projectPath, "-t:Clean;Build", "-consoleLoggerParameters:NoSummary;Verbosity=minimal"}, false)
	if err != nil {
		log.Debugf("`msbuild error:` %s", out)
	}
	return bytes.NewBuffer(out), err
}

// analyzeProject accepts a cli context and projectPath. The projectPath is the path to the .csproj/.vbproj file. It is
// an absolute path. Using this path, analyzeProject will attempt to add, clean, and build the project
// using dotnet. If unable to "add" the project, msbuild and nuget will be used to restore and clean/build the project.
func analyzeProjectDotNet(c *cli.Context, projectPath string) (io.Reader, error) {
	args := []string{"add", projectPath, "package", pkgSecurityCodeScan}

	// Add SecurityCodeScan package
	if c.IsSet(flagSecurityCodeScanVersion) {
		args = append(args, "-v", c.String(flagSecurityCodeScanVersion))
	}

	// Attempt to add project using dotnet
	out, err := run(dotnetCmd, args, false)
	if err != nil {
		log.Debugf("dotnet add error:\n%s", out)
		return nil, err
	}

	// clean project using dotnet
	out, err = run(dotnetCmd, []string{"clean", projectPath}, false)
	if err != nil {
		log.Errorf("dotnet clean error:\n%s", out)
		return nil, err
	}

	// build using dotnet
	out, err = run(dotnetCmd, []string{"build", projectPath}, true)
	if err != nil {
		// Ignore exit error
		if _, isExitErr := err.(*exec.ExitError); !isExitErr {
			log.Errorf("dotnet build error:\n%s", out)
			return nil, err
		}
	}
	return bytes.NewBuffer(out), nil
}

func run(command string, args []string, errOut bool) ([]byte, error) {
	cmd := exec.Command(command, args...)
	cmd.Env = os.Environ()
	if errOut {
		cmd.Stderr = os.Stderr
		return cmd.Output()
	}
	return cmd.CombinedOutput()
}

func findProjects(dir string, filter *pathfilter.Filter) ([]string, error) {
	var projects []string

	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		relPath, err := filepath.Rel(dir, path)
		if err != nil {
			return err
		}

		if filter.IsExcluded(relPath) {
			if info.IsDir() {
				return filepath.SkipDir
			}

			return nil
		}

		ext := filepath.Ext(path)
		if ext == ".csproj" || ext == ".vbproj" {
			pathDir := filepath.Dir(path)
			if pathDir != dir {
				log.Infof("Found project in %s", filepath.Dir(path))
			}
			projects = append(projects, path)
			return filepath.SkipDir
		}
		return nil
	})

	return projects, err
}

func findSolutions(dir string, filter *pathfilter.Filter) ([]string, error) {
	var solutions []string

	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		relPath, err := filepath.Rel(dir, path)
		if err != nil {
			return err
		}

		if filter.IsExcluded(relPath) {
			if info.IsDir() {
				return filepath.SkipDir
			}

			return nil
		}

		ext := filepath.Ext(path)
		if ext == ".sln" {
			log.Infof("Found solution %s", path)
			solutions = append(solutions, path)
		}
		return nil
	})

	return solutions, err
}
