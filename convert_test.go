package main

import (
	"fmt"
	"os"
	"reflect"
	"strings"
	"testing"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v2"
	"gitlab.com/gitlab-org/security-products/analyzers/ruleset"
	"gitlab.com/gitlab-org/security-products/analyzers/security-code-scan/v2/metadata"
)

func init() {
	os.Setenv("CI_PROJECT_DIR", "/tmp/app")
}

const testInput = `Microsoft (R) Build Engine version 15.7.177.53362 for .NET Core
Copyright (C) Microsoft Corporation. All rights reserved.

  Restore completed in 45.11 ms for /tmp/app/app.csproj.
WeakRandom.cs(7,16): warning SCS0005: Weak random generator [/tmp/app/app.csproj]
  app -> /tmp/app/bin/Debug/netcoreapp2.0/app.dll

Build succeeded.

WeakRandom.cs(7,16): warning SCS0005: Weak random generator [/tmp/app/app.csproj]
    1 Warning(s)
    0 Error(s)

Time Elapsed 00:00:01.69`

const multiTestInput = `Microsoft (R) Build Engine version 15.7.177.53362 for .NET Core
Copyright (C) Microsoft Corporation. All rights reserved.

  Restore completed in 45.11 ms for /tmp/app/app.csproj.
WeakRandom.cs(7,16): warning SCS0005: Weak random generator [/tmp/app/app.csproj]
  app -> /tmp/app/bin/Debug/netcoreapp2.0/app.dll

Build succeeded.

WeakRandom.cs(7,16): warning SCS0005: Weak random generator [/tmp/app/app.csproj]
    1 Warning(s)
    0 Error(s)

Time Elapsed 00:00:01.69
Microsoft (R) Build Engine version 15.7.177.53362 for .NET Core
Copyright (C) Microsoft Corporation. All rights reserved.

  Restore completed in 45.11 ms for /tmp/app/app.csproj.
WeakRandom.cs(8,17): warning SCS0007: Weak random generator [/tmp/app/app.csproj]
  app -> /tmp/app/bin/Debug/netcoreapp2.0/app.dll

Build succeeded.

WeakRandom.cs(9,18): warning SCS0008: Weak random generator [/tmp/app/app.csproj]
    1 Warning(s)
    0 Error(s)

Time Elapsed 00:00:01.69`

func TestUniqueMatches(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name     string
		input    string
		expected []string
	}{
		{"single project", testInput,
			[]string{
				"WeakRandom.cs(7,16): warning SCS0005: Weak random generator [/tmp/app/app.csproj]",
			},
		},
		{"multi project", multiTestInput,
			[]string{
				"WeakRandom.cs(7,16): warning SCS0005: Weak random generator [/tmp/app/app.csproj]",
				"WeakRandom.cs(8,17): warning SCS0007: Weak random generator [/tmp/app/app.csproj]",
				"WeakRandom.cs(9,18): warning SCS0008: Weak random generator [/tmp/app/app.csproj]",
			},
		},
	}

	for _, tt := range tests {
		tt := tt // NOTE: https://github.com/golang/go/wiki/CommonMistakes#using-goroutines-on-loop-iterator-variables
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			got := uniqueScan(strings.NewReader(tt.input))
			if !reflect.DeepEqual(tt.expected, got) {
				t.Errorf("Expected match to be\n%#v\nbut got\n%#v", tt.expected, got)
			}
		})
	}
}

func TestParse(t *testing.T) {
	want := &SecWarning{
		SourceFile:  "WeakRandom.cs",
		Line:        7,
		Column:      16,
		RuleID:      "SCS0005",
		Message:     "Weak random generator",
		ProjectFile: "/tmp/app/app.csproj",
	}
	got, _ := parse("WeakRandom.cs(7,16): warning SCS0005: Weak random generator [/tmp/app/app.csproj]")
	if !reflect.DeepEqual(want, got) {
		t.Errorf("Expected\n%#v\nbut got\n%#v", want, got)
	}
}

func TestConvert(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name     string
		input    string
		expected *report.Report
	}{
		{"single project", testInput, newVuln([]succinctVuln{{"SCS0005", 7}})},
		{"multi project", multiTestInput, newVuln([]succinctVuln{{"SCS0005", 7}, {"SCS0007", 8}, {"SCS0008", 9}})},
	}

	for _, tt := range tests {
		tt := tt // NOTE: https://github.com/golang/go/wiki/CommonMistakes#using-goroutines-on-loop-iterator-variables
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			r := strings.NewReader(tt.input)

			got, err := convert(r, "app")
			if err != nil {
				t.Fatal(err)
			}
			if !reflect.DeepEqual(tt.expected, got) {
				t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", tt.expected, got)
			}
		})
	}
}

func TestFilepath(t *testing.T) {
	tests := []struct {
		name     string
		result   Result
		expected string
	}{
		{
			name: "csproj to sibling source",
			result: Result{
				SecWarning: &SecWarning{
					ProjectFile: "/tmp/project/app/app.csproj",
					SourceFile:  "WeakRandom.cs",
				},
				Root: "/tmp/project",
			},
			expected: "app/WeakRandom.cs",
		},
		{
			name: "csproj to nested source",
			result: Result{
				SecWarning: &SecWarning{
					ProjectFile: "/tmp/project/app.csproj",
					SourceFile:  "/tmp/project/subdir/src.cs",
				},
				Root: "/tmp/project",
			},
			expected: "subdir/src.cs",
		},
		{
			name: "csproj to sibling nested source",
			result: Result{
				SecWarning: &SecWarning{
					ProjectFile: "/tmp/project/WebGoat/WebGoat.NET.csproj",
					SourceFile:  "/tmp/project/WebGoat/WebGoatCoins/Orders.aspx.cs",
				},
				Root: "/tmp/project",
			},
			expected: "WebGoat/WebGoatCoins/Orders.aspx.cs",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.result.Filepath()
			if !reflect.DeepEqual(tt.expected, got) {
				t.Errorf("Expected match to be\n%#v\nbut got\n%#v", tt.expected, got)
			}
		})
	}
}

func NewResult(root string, projectFile string, sourceFile string) *Result {
	warning := &SecWarning{
		ProjectFile: projectFile,
		SourceFile:  sourceFile,
	}

	return &Result{
		SecWarning: warning,
		Root:       root,
	}
}

type succinctVuln struct {
	name string
	line int
}

func newVuln(svulns []succinctVuln) *report.Report {
	vulns := make([]report.Vulnerability, len(svulns))

	for idx, succinctVuln := range svulns {
		vulns[idx] = report.Vulnerability{
			Category: "sast",
			Scanner: report.Scanner{
				ID:   "security_code_scan",
				Name: "Security Code Scan",
			},
			Name:    "Weak random generator",
			Message: "Weak random generator",
			Location: report.Location{
				File:      "WeakRandom.cs",
				LineStart: succinctVuln.line,
			},
			Identifiers: []report.Identifier{
				{
					Type:  "security_code_scan_rule_id",
					Name:  succinctVuln.name,
					Value: succinctVuln.name,
					URL:   "https://security-code-scan.github.io/#" + succinctVuln.name,
				},
			},
			CompareKey: fmt.Sprintf("WeakRandom.cs:%d:%s", succinctVuln.line, succinctVuln.name),
		}
	}

	return &report.Report{
		Version:         report.CurrentVersion(),
		Vulnerabilities: vulns,
		Remediations:    []report.Remediation{},
		DependencyFiles: []report.DependencyFile{},
		Analyzer:        metadata.AnalyzerName,
		Config:          ruleset.Config{Path: ruleset.PathSAST},
	}
}
